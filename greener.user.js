/**
 * Created by johnji on 6/4/14.  For employee/internal use only.
 */
// ==UserScript==
// @name          Greener v01.12
// @namespace     http://www.citrix.com/gmscripts
// @description   Green Stories (Jira Hosted)
// @include       https://citrixwebteam.jira.com/secure/RapidBoard.jspa*
// ==/UserScript==

// v1.12 -- added Bug type support

(function() {

	var jQuery = (typeof unsafeWindow == 'undefined' ? window : unsafeWindow).jQuery;
	var $ = jQuery(document);

	//https://citrixwebteam.jira.com/rest/api/latest/search
	//?fields=key&jql=type=Story and project=Core and Greened ~ Yes and key in (CORE-2560,CORE-3418, CORE-3439)
	var url    = "https://citrixwebteam.jira.com/rest/api/latest/search";
	url 	  += "?fields=key&jql=(type=Story or type=Bug) and (Greened=Yes) and key in ";
	
	$.ready(function($){

		c('started');

		var eMap = {};

		//lazy way of kicking this off -- should be looking for an event.
		setTimeout(function(){

			$('a.js-detailview').each(function(i, obj) {				
   			
   				eMap[$(obj).text()] = obj;
			
			});

			url += "("+ Object.keys(eMap).join(",") +")";
            
            c(url);
            
			$.get( url , function(data){

				var issues = data['issues'];

				for(var i=0; i< issues.length; i++){

					var k = issues[i];

					if(issues[i].key in eMap){
						$( eMap[issues[i].key] ).append("&nbsp;<strong>[ G ]</strong>");	
					}
				}

			}).error(function(err){

                var msg = eval("(" + err.responseText + ")");            
				console.log(msg);

			}).always(function(){
				
				c('completed.');
			});

		}, 5000);

	});

	function c(str){
		console.log('greener> '+ str);
	}

})();