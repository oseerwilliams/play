If you want to see what stories are greened in JIRA's Agile boards install the following greasemonkey script.

Caveats: It will only working in Firefox at the moment.  Limited to adding 50 [ G ]’s only right now. 

1. Install the Greasemonkey plugin for firefox.
2. Download greener.user.js to your desktop, then drag it onto an open firefox window.
3. Browse to your Agile dashboard and after a deley (5 secs) you should see a [ G ] without modifying the title of the story.
4. You can disable any time w/ the Greasemonkey manager in the tool bar. 

Let me know if you have feedback if it’s useful i’ll spend more time on it.


Thanks,
John

Ps. Remember that it’s a hack that loads at page load, so modifying the data on the screen doesn’t re-trigger the script (just reload the screen to reload the [ G ]’s).
